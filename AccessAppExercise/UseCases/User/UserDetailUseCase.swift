import Foundation
import RxSwift

class UserDetailUseCase {
    
    private let userRepository: UserRepositoryProtocol
    
    init(userRepository: UserRepositoryProtocol = UserRepository()) {
        self.userRepository = userRepository
    }
    
    func invoke(login: String) -> Observable<UserDetailModel?> {
        return self.userRepository.fetchUserDetail(login).map { (aaeUserDetail) in
            return UserDetailModel(user: aaeUserDetail)
        }
    }
}
