import Foundation
import RxSwift

class UserListUseCase {
    
    private let userRepository: UserRepositoryProtocol
    
    init(userRepository: UserRepositoryProtocol = UserRepository()) {
        self.userRepository = userRepository
    }
    
    func invoke(since: Int) -> Observable<([UserModel])> {
        return self.userRepository.fetchUserList(since).map { (aaeUsers) in
            return aaeUsers.map { UserModel(user: $0) }
        }
    }
}
