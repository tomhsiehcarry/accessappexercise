import UIKit
import SDWebImage

class UserCell: UITableViewCell, ReusableView {
    
    private var avatar_IMG: UIImageView!
    private var login_LBL: UILabel!
    private var siteAdminView: UIView!
    private var siteAdmin_LBL: UILabel!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        avatar_IMG.image = nil
    }
    
    private func setupLayout() {
        avatar_IMG = UIImageView(frame: .zero)
        avatar_IMG?.layer.masksToBounds = true
        avatar_IMG?.layer.cornerRadius = 22
        
        login_LBL = UILabel()
        login_LBL?.textColor = UIColor.black
        login_LBL?.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        
        siteAdmin_LBL = UILabel()
        siteAdmin_LBL?.textColor = UIColor.white
        siteAdmin_LBL?.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        siteAdmin_LBL.text = "Staff"
        
        siteAdminView = UIView()
        siteAdminView?.backgroundColor = .systemBlue
        siteAdminView?.addSubview(siteAdmin_LBL!)
        siteAdminView.layer.masksToBounds = true
        siteAdminView.layer.cornerRadius = 12
        
        siteAdmin_LBL.translatesAutoresizingMaskIntoConstraints = false
        siteAdmin_LBL.leadingAnchor.constraint(equalTo: siteAdminView.leadingAnchor, constant: 8).isActive = true
        siteAdmin_LBL.trailingAnchor.constraint(equalTo: siteAdminView.trailingAnchor, constant: -8).isActive = true
        siteAdmin_LBL.topAnchor.constraint(equalTo: siteAdminView.topAnchor, constant: 3).isActive = true
        siteAdmin_LBL.bottomAnchor.constraint(equalTo: siteAdminView.bottomAnchor, constant: -3).isActive = true
        
        let stackView = UIStackView()
        stackView.alignment = .leading
        stackView.axis = .vertical
        stackView.spacing = 4
        
        stackView.addArrangedSubview(login_LBL!)
        stackView.addArrangedSubview(siteAdminView!)
        
        self.contentView.addSubview(stackView)
        self.contentView.addSubview(avatar_IMG!)
        
        avatar_IMG.translatesAutoresizingMaskIntoConstraints = false
        stackView.translatesAutoresizingMaskIntoConstraints  = false
        
        avatar_IMG.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16).isActive = true
        avatar_IMG.widthAnchor.constraint(equalToConstant: 44).isActive = true
        avatar_IMG.heightAnchor.constraint(equalToConstant: 44).isActive = true
        avatar_IMG.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16).isActive = true
        avatar_IMG.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16).isActive = true
        
        stackView.leftAnchor.constraint(equalTo: avatar_IMG.rightAnchor, constant: 8).isActive = true
        stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8).isActive = true
        stackView.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor, constant: 16).isActive = true
        stackView.bottomAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.bottomAnchor, constant: -16).isActive = true
    }
    
    func configure(user: UserModel) {
        self.avatar_IMG.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.avatar_IMG.sd_setImage(with: URL(string: user.avatarUrl), completed: nil)
        
        self.login_LBL.text = user.login
        
        self.siteAdminView.isHidden = !user.siteAdmin
    }
}
