import UIKit
import RxSwift

class UserListViewController: UIViewController {
    
    private let viewModel = UserListViewModel()
    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = .white
        tableView.register(UserCell.self)
        return tableView
    }()
    
    private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "User List"
        self.view.backgroundColor = .white
        
        tableView.delegate = self
        tableView.dataSource = self
        
        setupLayout()
        bindViewModel()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
    }
    
    private func setupLayout() {
        
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    
    private func bindViewModel() {
        
        viewModel.statusRelay.asObservable().subscribe(onNext: { [unowned self] (status) in
            switch status {
            case .Requesting:
                self.tableView.tableFooterView = createSpinerFooter()
            case .Normal:
                self.tableView.tableFooterView = nil
                self.tableView.reloadData()
            case .Error:
                self.tableView.tableFooterView = nil
                print("error")
            }
        }).disposed(by: disposeBag)
    }
    
    private func createSpinerFooter() -> UIView {
        let footerView = UIView(frame: .init(x: 0, y: 0, width: view.frame.size.width, height: 100))
        let spinner = UIActivityIndicatorView()
        spinner.center = footerView.center
        footerView.addSubview(spinner)
        spinner.startAnimating()
        
        return footerView
    }
}

extension UserListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.usersRelay.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        let user = viewModel.usersRelay.value[row]
        
        let cell: UserCell = tableView.dequeueReusableCell(foIindexPath: indexPath)
        
        cell.configure(user: user)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        let user = viewModel.usersRelay.value[row]
        
        let vc = UserDetailViewController(login: user.login)
        self.show(vc, sender: self)
    }
}

extension UserListViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let position = scrollView.contentOffset.y
        if position > (tableView.contentSize.height - 100 - scrollView.frame.size.height ) {
            viewModel.nextPageSubject.onNext(())
        }
    }
}
