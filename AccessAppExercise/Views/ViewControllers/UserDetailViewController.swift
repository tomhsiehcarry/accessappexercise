import UIKit
import RxSwift
import RxRelay
import SDWebImage

class UserDetailViewController: UIViewController {
    //layout
    private let avatar_IMG: UIImageView = {
        let image = UIImageView(frame: .zero)
        return image
    }()
    
    private let name_LBL: UILabel = {
        let label = UILabel(frame: .zero)
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 24, weight: .regular)
        label.textColor = .gray
        return label
    }()
    
    private let bio_LBL: UILabel = {
        let label = UILabel(frame: .zero)
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        label.textColor = .gray
        label.numberOfLines = 0
        return label
    }()
    
    private let dividerView: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = .gray
        return view
    }()
    
    
    //info
    private let login_LBL: UILabel = {
        let login_LBL = UILabel()
        login_LBL.textColor = UIColor.black
        login_LBL.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        return login_LBL
    }()
    
    private let siteAdmin_LBL: UILabel = {
        let siteAdmin_LBL = UILabel()
        siteAdmin_LBL.textColor = UIColor.white
        siteAdmin_LBL.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        siteAdmin_LBL.text = "Staff"
        return siteAdmin_LBL
    }()
    
    private let siteAdminView: UIView = {
        let siteAdminView = UIView()
        siteAdminView.backgroundColor = .systemBlue
        siteAdminView.layer.masksToBounds = true
        siteAdminView.layer.cornerRadius = 12
        return siteAdminView
    }()
    
    //location
    private let location_LBL: UILabel = {
        let location_LBL = UILabel()
        location_LBL.textColor = UIColor.black
        location_LBL.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        return location_LBL
    }()
    
    //blog
    private let blog_LBL: UILabel = {
        let blog_LBL = UILabel()
        blog_LBL.textColor = UIColor.black
        blog_LBL.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        return blog_LBL
    }()
    
    private var viewModel: UserDetailViewModel?
    private let disposeBag = DisposeBag()
    
    init(login: String) {
        self.viewModel = UserDetailViewModel(login: login)
        super.init(nibName: nil, bundle: nil)
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.viewModel = nil
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        bindViewModel()
    }
    
    private func setupLayout() {
        view.backgroundColor = .white
        
        setupTopViewContainer()
        setupBottomViewContainer()
    }
    
    private func setupTopViewContainer() {
        view.addSubview(avatar_IMG)
        view.addSubview(name_LBL)
        view.addSubview(bio_LBL)
        view.addSubview(dividerView)
        
        avatar_IMG.translatesAutoresizingMaskIntoConstraints = false
        name_LBL.translatesAutoresizingMaskIntoConstraints = false
        bio_LBL.translatesAutoresizingMaskIntoConstraints = false
        dividerView.translatesAutoresizingMaskIntoConstraints = false
        
        avatar_IMG.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        avatar_IMG.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 12).isActive = true
        avatar_IMG.widthAnchor.constraint(equalToConstant: Constants.WIDTH / 2).isActive = true
        avatar_IMG.heightAnchor.constraint(equalToConstant: Constants.WIDTH / 2).isActive = true
        avatar_IMG.layer.masksToBounds = true
        avatar_IMG.layer.cornerRadius  = Constants.WIDTH / 4
        
        name_LBL.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        name_LBL.topAnchor.constraint(equalTo: avatar_IMG.bottomAnchor, constant: 6).isActive = true
        name_LBL.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        name_LBL.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true
        name_LBL.heightAnchor.constraint(equalToConstant: 32).isActive = true
        
        bio_LBL.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        bio_LBL.topAnchor.constraint(equalTo: name_LBL.bottomAnchor, constant: 6).isActive = true
        bio_LBL.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        bio_LBL.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true
        bio_LBL.heightAnchor.constraint(equalToConstant: 24).isActive = true
        
        dividerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        dividerView.topAnchor.constraint(equalTo: bio_LBL.bottomAnchor, constant: 6).isActive = true
        dividerView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        dividerView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        dividerView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true
    }
    
    private func setupBottomViewContainer() {
        let bottomStackView = UIStackView()
        bottomStackView.alignment = .leading
        bottomStackView.axis = .vertical
        bottomStackView.distribution = .equalSpacing
        bottomStackView.spacing = 44
        
        view.addSubview(bottomStackView)
        bottomStackView.translatesAutoresizingMaskIntoConstraints = false
        bottomStackView.topAnchor.constraint(equalTo: dividerView.bottomAnchor, constant: 24).isActive = true
        bottomStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        bottomStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true
        
        bottomStackView.addArrangedSubview(createInfoView())
        bottomStackView.addArrangedSubview(createLocationView())
        bottomStackView.addArrangedSubview(createBlogView())
    }
    
    private func createInfoView() -> UIView {
        let infoView = UIView(frame: .zero)
        infoView.heightAnchor.constraint(equalToConstant: 32).isActive = true
        
        let imageView = UIImageView(frame: .zero)
        let largeConfig = UIImage.SymbolConfiguration(pointSize: 32, weight: .regular)
        imageView.tintColor = .black
        imageView.image = UIImage.init(systemName: "person.fill", withConfiguration: largeConfig)
        
        siteAdminView.addSubview(siteAdmin_LBL)
        siteAdmin_LBL.translatesAutoresizingMaskIntoConstraints = false
        siteAdmin_LBL.leadingAnchor.constraint(equalTo: siteAdminView.leadingAnchor, constant: 8).isActive = true
        siteAdmin_LBL.trailingAnchor.constraint(equalTo: siteAdminView.trailingAnchor, constant: -8).isActive = true
        siteAdmin_LBL.topAnchor.constraint(equalTo: siteAdminView.topAnchor, constant: 3).isActive = true
        siteAdmin_LBL.bottomAnchor.constraint(equalTo: siteAdminView.bottomAnchor, constant: -3).isActive = true
        
        let stackView = UIStackView()
        stackView.alignment = .leading
        stackView.axis = .vertical
        stackView.spacing = 4
        
        stackView.addArrangedSubview(login_LBL)
        stackView.addArrangedSubview(siteAdminView)
        
        infoView.addSubview(imageView)
        infoView.addSubview(stackView)
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        imageView.centerYAnchor.constraint(equalTo: infoView.centerYAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: infoView.leadingAnchor).isActive = true
        
        stackView.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 16).isActive = true
        stackView.trailingAnchor.constraint(equalTo: infoView.trailingAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: infoView.topAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: infoView.bottomAnchor).isActive = true
        
        return infoView
    }
    
    private func createLocationView() -> UIView {
        let locationView = UIView(frame: .zero)
        locationView.heightAnchor.constraint(equalToConstant: 32).isActive = true
        
        let imageView = UIImageView(frame: .zero)
        let largeConfig = UIImage.SymbolConfiguration(pointSize: 32, weight: .regular)
        imageView.tintColor = .black
        imageView.image = UIImage.init(systemName: "location.fill", withConfiguration: largeConfig)
        
        locationView.addSubview(imageView)
        locationView.addSubview(location_LBL)
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        location_LBL.translatesAutoresizingMaskIntoConstraints = false
        
        imageView.centerYAnchor.constraint(equalTo: locationView.centerYAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: locationView.leadingAnchor).isActive = true
        
        location_LBL.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 8).isActive = true
        location_LBL.trailingAnchor.constraint(equalTo: locationView.trailingAnchor, constant: -8).isActive = true
        location_LBL.centerYAnchor.constraint(equalTo: locationView.centerYAnchor).isActive = true
        
        return locationView
    }
    
    private func createBlogView() -> UIView {
        let blogView = UIView(frame: .zero)
        blogView.heightAnchor.constraint(equalToConstant: 32).isActive = true
        
        let imageView = UIImageView(frame: .zero)
        let largeConfig = UIImage.SymbolConfiguration(pointSize: 32, weight: .regular)
        imageView.tintColor = .black
        imageView.image = UIImage.init(systemName: "link", withConfiguration: largeConfig)
        
        blogView.addSubview(imageView)
        blogView.addSubview(blog_LBL)
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        blog_LBL.translatesAutoresizingMaskIntoConstraints = false
        
        imageView.centerYAnchor.constraint(equalTo: blogView.centerYAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: blogView.leadingAnchor).isActive = true
        
        blog_LBL.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 8).isActive = true
        blog_LBL.trailingAnchor.constraint(equalTo: blogView.trailingAnchor, constant: -8).isActive = true
        blog_LBL.centerYAnchor.constraint(equalTo: blogView.centerYAnchor).isActive = true
        
        return blogView
    }
    
    private func bindViewModel() {
        viewModel?.statusRelay.asObservable().subscribe(onNext: { (status) in
            switch status {
            case .Requesting:
                HUD.show()
            case .Normal:
                HUD.dismiss()
            case .Error:
                HUD.dismiss()
                print("error")
            }
        }).disposed(by: disposeBag)
        
        viewModel?.userDetailRelay.asObservable().subscribe(onNext: { [unowned self] (userDetail) in
            
            guard let userDetail = userDetail else { return }
            
            self.avatar_IMG.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.avatar_IMG.sd_setImage(with: URL(string: userDetail.avatarUrl), completed: nil)
            self.name_LBL.text = userDetail.name
            self.bio_LBL.text = userDetail.bio
            
            self.login_LBL.text = userDetail.login
            self.siteAdminView.isHidden = !userDetail.siteAdmin
            
            self.location_LBL.text = userDetail.location
            self.blog_LBL.text = userDetail.blog
            
        }).disposed(by: disposeBag)
    }
}
