import Foundation
import RxSwift
import RxCocoa

class UserDetailViewModel {
    
    public let loginRelay = BehaviorRelay<String>(value: "")
    
    //output
    public let statusRelay = BehaviorRelay<Status>(value: .Requesting)
    public let userDetailRelay = BehaviorRelay<UserDetailModel?>(value: nil)
    
    private let disposeBag = DisposeBag()
    private let useCase = UserDetailUseCase()
    
    init(login: String) {
        self.loginRelay.accept(login)
        
        loginRelay.flatMap { [unowned self] (login) -> Observable<UserDetailModel?> in
            self.statusRelay.accept(.Requesting)
            return self.useCase.invoke(login: login)
        }.catch ({ (_) in
            self.statusRelay.accept(.Error)
            return Observable.just(nil)
        }).subscribe(onNext: { [unowned self] (userDetail) in
            self.userDetailRelay.accept(userDetail)
            self.statusRelay.accept(.Normal)
        }).disposed(by: disposeBag)
    }
}
