import Foundation
import RxSwift
import RxCocoa

class UserListViewModel {
    
    //input
    public let nextPageSubject = PublishSubject<Void>()
    
    //output
    public let statusRelay = BehaviorRelay<Status>(value: .Requesting)
    public let usersRelay = BehaviorRelay<[UserModel]>(value: [])
    
    private let sinceRelay = BehaviorRelay<Int>(value: 0)
    private let disposeBag = DisposeBag()
    private let useCase = UserListUseCase()
    
    init() {
        bind()
    }
    
    private func bind() {
        
        nextPageSubject.asObserver().subscribe(onNext: { [unowned self] in
            if statusRelay.value == .Requesting || usersRelay.value.count >= 100 { return }
            
            statusRelay.accept(.Requesting)
            let since = usersRelay.value.last?.id ?? 0
            self.sinceRelay.accept(since + 1)
            
        }).disposed(by: disposeBag)
        
        sinceRelay.flatMap { [unowned self] (since) -> Observable<[UserModel]> in
            return self.useCase.invoke(since: since)
        }.flatMap { [unowned self] (newUsers) -> Observable<Status> in
            var users = self.usersRelay.value
            users += newUsers
            self.usersRelay.accept(users)
            return Observable.just(.Normal)
        }
        .catch( { (error) -> Observable<Status> in
           return Observable.just(.Error)
        }).bind(to: statusRelay).disposed(by: disposeBag)
    }
}
