import Foundation

struct UserDetailModel {
    var login: String
    var id: Int
    var avatarUrl: String
    var siteAdmin: Bool
    var name: String
    var bio: String
    var location: String
    var blog: String

    init(user: AAEUserDetail) {
        self.login = user.login ?? ""
        self.id = user.id ?? 0
        self.avatarUrl = user.avatarUrl ?? ""
        self.siteAdmin = user.siteAdmin ?? false
        self.name = user.name ?? ""
        self.bio = user.bio ?? ""
        self.location = user.location ?? ""
        self.blog = user.blog ?? ""
    }
}
