import Foundation

struct UserModel {
    var login: String
    var id: Int
    var avatarUrl: String
    var siteAdmin: Bool

    init(user: AAEUser) {
        self.login = user.login
        self.id = user.id
        self.avatarUrl = user.avatarUrl
        self.siteAdmin = user.siteAdmin
    }
}
