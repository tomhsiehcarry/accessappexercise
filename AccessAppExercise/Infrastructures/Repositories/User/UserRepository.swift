import Foundation
import RxSwift

protocol UserRepositoryProtocol {
    func fetchUserList(_ since: Int) -> Observable<[AAEUser]>
    func fetchUserDetail(_ login: String) -> Observable<AAEUserDetail>
}

class UserRepository: UserRepositoryProtocol {
    
    private let aaeApi = AAEAPIClientManager()
    
    func fetchUserList(_ since: Int) -> Observable<[AAEUser]> {
        
        return Observable<[AAEUser]>.create { [weak self] observer -> Disposable in
            
            let parameters = AAEUserListParameters()
            parameters.since = since
            
            self?.aaeApi.client.fetchUserList(parameters, completion: { result in
                switch result {
                case .Success(let users):
                    observer.onNext(users)
                    observer.onCompleted()
                case .Failure(let error):
                    observer.onError(error)
                }
            })
            
            return Disposables.create()
        }
    }
    
    func fetchUserDetail(_ login: String) -> Observable<AAEUserDetail> {
        
        return Observable<AAEUserDetail>.create { [weak self] observer -> Disposable in
            
            self?.aaeApi.client.fetchUserDetail(login, completion: { result in
                switch result {
                case .Success(let detail):
                    observer.onNext(detail)
                    observer.onCompleted()
                case .Failure(let error):
                    observer.onError(error)
                }
            })
            
            return Disposables.create()
        }
    }
}
