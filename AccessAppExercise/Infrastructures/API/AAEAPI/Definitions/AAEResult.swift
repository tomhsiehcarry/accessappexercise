enum AAEResult<T, U: Swift.Error> {
    case Success(T)
    case Failure(U)
}
