import Foundation

class AAEUserListParameters: AAEParametersType {
    
    public var since: Int? = 0
    public var perPage: Int? = 20
    
    public let needsAccessToken = false
    public let convertsArrayToCommaSeparatedString = false
    
    func toQueryDictionary() -> [String : Any] {
        var parameters = [String: Any]()
        
        if let value = perPage {
            parameters["per_page"] = value
        }
        
        if let value = since {
            parameters["since"] = value
        }
        
        return parameters
    }
}
