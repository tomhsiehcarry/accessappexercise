import Alamofire
import ObjectMapper

enum AAEEndpoint {
    
    case FetchUserList
    case FetchUserDetail(login: String)
    
    private var string: String {
        switch self {
        case .FetchUserList:
            return "/users"
        case .FetchUserDetail(let login):
            return "/users/\(login)"
        }
    }
    
    // MARK: - Internal
    func request<T: Mappable>(_ client: AAEClient, method: HTTPMethod, parameters: AAEParametersType?, completion: @escaping (AAEResult<T, AAEError>) -> Void) {
        
        self.requestJson(client, method: method, parameters: parameters) { result in
            switch result {
            case .Success(let json):
                if let model = Mapper<T>().map(JSONObject: json) {
                    completion(.Success(model))
                } else {
                    completion(.Failure(.JSONParse))
                }
            case .Failure(let error):
                completion(.Failure(error))
            }
        }
    }
    
    /// Response with Mappable Array
    func request<T: Mappable>(_ client: AAEClient, method: HTTPMethod, parameters: AAEParametersType?, completion: @escaping (AAEResult<[T], AAEError>) -> Void) {
        self.requestJson(client, method: method, parameters: parameters) { result in
            switch result {
            case .Success(let json):
                if let model = Mapper<T>().mapArray(JSONObject: json) {
                    completion(.Success(model))
                } else {
                    completion(.Failure(.JSONParse))
                }
            case .Failure(let error):
                completion(.Failure(error))
            }
        }
    }
    
    /// 通過處理使用指定方法和參數獲得的JSON，然後將其返回。
    private func requestJson(_ client: AAEClient, method: HTTPMethod, parameters: AAEParametersType?, completion: @escaping (AAEResult<Any, AAEError>) -> Void) {
        
        let request = try! URLRequest(url: client.setting.baseURL + string, method: method)
        let mutableURLRequest = try! AAEMutableURLRequestGenerator.generate(request, with: modifyParameters(client.setting, parameters: parameters))
        
        client.setting.manager.request(mutableURLRequest).responseJSON { response in
            self.handleResponse(client, response: response, completion: completion)
        }
    }
    
    /// `Alamofire.request(...).responseJSON(...)` 取得Response，並轉回Json格式
    private func handleResponse(_ client: AAEClient, response: AFDataResponse<Any>, completion: (AAEResult<Any, AAEError>) -> Void) {
        let statusCode = response.response?.statusCode ?? 0
        
        // 如果是服務結束代碼，則顯示結束消息並註銷
        if statusCode == AAEErrorCode.EndOfService.rawValue {
            completion(.Failure(.EndOfService))
            client.endOfService?()
            return
        }
        
        // 維護模式的判斷（由於響應為空，因此似乎只能通過text / html來判斷）
        let contentType = response.response?.allHeaderFields["Content-Type"] as? String ?? ""
        guard !contentType.contains("text/html") else {
            completion(.Failure(.ServerMaintenance))
            client.serverMaintenanceOccured?()
            return
        }
        
        switch response.result {
        case .success(let data):
            completion(.Success(data))
        case .failure(let error):
            completion(.Failure(.Connection(error)))
        }
    }
    
    private func modifyParameters(_ setting: AAEClient.Setting, parameters: AAEParametersType?) -> [String: Any] {
        var newParameters: [String: Any] = parameters?.toQueryDictionary() ?? [:]
        
        for (key, value) in newParameters {
            if let array = value as? [Any] {
                
                guard array.count > 0 else {
                    newParameters.removeValue(forKey: key)
                    continue
                }
                
                if parameters?.convertsArrayToCommaSeparatedString == true {
                    newParameters[key] = array.map { String(describing: $0) }.joined(separator: ",")
                }
            }
        }
        
        return newParameters
    }
}
