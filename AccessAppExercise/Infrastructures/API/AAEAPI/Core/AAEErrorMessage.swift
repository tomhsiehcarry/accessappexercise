import ObjectMapper

class AAEErrorMessage: Mappable {

    var errors: [Error]?

    var errorCodes: [Error.Code] {
        return errors?.compactMap { $0.code } ?? []
    }

    var aaeApiError: AAEAPIError?

    required init?(map: Map) {}

    func mapping(map: Map) {
        errors <- map["errors"]
    }

    class Error: Mappable {

        enum Code: Int {
            case RequestParameterError = 400
            case InvalidAccessToken = 401
            case ContainsInvalidURL = 404
            case ServerError = 500
        }

        var code: Code?
        var reason: String?
        var details: [String: String]?

        required init?(map: Map) {}

        func mapping(map: Map) {
            code <- map["code"]
            reason <- map["reason"]
            details <- map["details"]
        }
    }
}

class AAEAPIError {
    var toNSError: NSError

    init(request: URLRequest?, response: HTTPURLResponse?) {
        let title = String(format: "%@ %@://%@%@", request?.httpMethod ?? "", request?.url?.scheme ?? "", request?.url?.host ?? "", request?.url?.path ?? "", String(response?.statusCode ?? 0))
        let userInfo = ["query": request?.url?.query ?? ""]

        toNSError = NSError(domain: title, code: response?.statusCode ?? 0, userInfo: userInfo)
    }
}
