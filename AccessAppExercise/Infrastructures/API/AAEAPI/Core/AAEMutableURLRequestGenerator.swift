import Alamofire

class AAEMutableURLRequestGenerator {

    static func generate(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {

        func encodesParametersInURL(with method: HTTPMethod) -> Bool {
            switch method {
            case .get, .head, .delete:
                return true
            default:
                return false
            }
        }

        func query(_ parameters: [String: Any]) -> String {
            var components: [(String, String)] = []

            for key in parameters.keys.sorted(by: <) {
                let value = parameters[key]!
                components += queryComponents(fromKey: key, value: value)
            }

            return components.map { "\($0)=\($1)" }.joined(separator: "&")
        }

        func queryComponents(fromKey key: String, value: Any) -> [(String, String)] {
            var components: [(String, String)] = []

            if let dictionary = value as? [String: Any] {
                for (nestedKey, value) in dictionary {
                    components += queryComponents(fromKey: "\(key)[\(nestedKey)]", value: value)
                }
            } else if let array = value as? [Any] {
                for value in array {
                    components += queryComponents(fromKey: "\(key)", value: value)
                }
            } else if let value = value as? NSNumber {
                if value.isBool {
                    components.append((escape(key), escape((value.boolValue ? "1" : "0"))))
                } else {
                    components.append((escape(key), escape("\(value)")))
                }
            } else if let bool = value as? Bool {
                components.append((escape(key), escape((bool ? "1" : "0"))))
            } else {
                components.append((escape(key), escape("\(value)")))
            }

            return components
        }

        func escape(_ string: String) -> String {
            let generalDelimitersToEncode = ":#[]@"
            let subDelimitersToEncode = "!$&'()*+,;="

            var allowedCharacterSet = CharacterSet.urlQueryAllowed
            allowedCharacterSet.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")

            return string.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) ?? string
        }

        var urlRequest = try urlRequest.asURLRequest()

        guard let parameters = parameters else { return urlRequest }

        let method = HTTPMethod(rawValue: urlRequest.httpMethod ?? "GET")
        
        guard let url = urlRequest.url, encodesParametersInURL(with: method) else {
            throw AFError.parameterEncodingFailed(reason: .missingURL)
        }

        if var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false), !parameters.isEmpty {
            let percentEncodedQuery = (urlComponents.percentEncodedQuery.map { $0 + "&" } ?? "") + query(parameters)
            urlComponents.percentEncodedQuery = percentEncodedQuery
            urlRequest.url = urlComponents.url
        }

        return urlRequest
    }

}

extension NSNumber {
    fileprivate var isBool: Bool { return CFBooleanGetTypeID() == CFGetTypeID(self) }
}
