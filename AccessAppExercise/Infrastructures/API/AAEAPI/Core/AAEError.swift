enum AAEError: Swift.Error {
    case Connection(Swift.Error)
    case JSONParse
    case AAEAPI(AAEErrorMessage)
    case ServerMaintenance
    case EndOfService
    case AuthCodeFetchFailed
    case Unauthorized
    case Conflict(description: String)
    case Other(description: String)
}

enum AAEErrorCode: Int {
    case EndOfService = 410
    case Unauthorized = 401
    case Conflict = 409
}
