import Foundation
import ObjectMapper

class AAEUserDetail: Mappable {
    
    public var login: String?
    public var id: Int?
    public var avatarUrl: String?
    public var siteAdmin: Bool?
    public var name: String?
    public var bio: String?
    public var location: String?
    public var blog: String?
    
    public required init?(map: Map) { }
    
    public func mapping(map: Map) {
        login <- map["login"]
        id <- map["id"]
        avatarUrl <- map["avatar_url"]
        siteAdmin <- map["site_admin"]
        name <- map["name"]
        bio <- map["bio"]
        location <- map["location"]
        blog <- map["blog"]
    }
}
