import Foundation
import ObjectMapper

class AAEUser: Mappable {
    
    public var login: String!
    public var id: Int!
    public var avatarUrl: String!
    public var siteAdmin: Bool!
    
    public required init?(map: Map) { }
    
    public func mapping(map: Map) {
        login <- map["login"]
        id <- map["id"]
        avatarUrl <- map["avatar_url"]
        siteAdmin <- map["site_admin"]
    }
}
