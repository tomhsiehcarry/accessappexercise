protocol AAEParametersType {
    var needsAccessToken: Bool { get }

    var convertsArrayToCommaSeparatedString: Bool { get }

    func toQueryDictionary() -> [String: Any]
}
