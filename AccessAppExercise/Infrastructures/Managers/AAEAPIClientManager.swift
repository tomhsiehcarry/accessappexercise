import RxSwift
import Foundation

class AAEAPIClientManager {
    private let clientInMemory = AAEClient(baseURL: Constants.AAEAPI.baseURI)

    private let disposeBag = DisposeBag()

    var client: AAEClient {
        return clientInMemory
    }
}
