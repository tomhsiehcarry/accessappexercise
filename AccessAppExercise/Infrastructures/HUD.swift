import JGProgressHUD

struct HUD {

    private static let hud: JGProgressHUD = {
        let hud = JGProgressHUD(style: .dark)
        hud.interactionType = .blockAllTouches
        return hud
    }()

    static func show(interaction: JGProgressHUDInteractionType = .blockAllTouches) {
        if let view = Constants.topMostController?.view {
            hud.show(in: view)
            hud.interactionType = interaction
        }
    }

    static func show(interaction: JGProgressHUDInteractionType = .blockAllTouches, to view: UIView) {
        hud.show(in: view)
        hud.interactionType = interaction
    }

    static func dismiss() {
        hud.dismiss()
    }
}
