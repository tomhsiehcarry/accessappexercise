import Foundation

enum Status {
    case Normal, Requesting, Error
}
