import Foundation
import UIKit

struct Constants {
    static let WIDTH = UIScreen.main.bounds.width
    static let HEIGHT = UIScreen.main.bounds.height
    
    struct AAEAPI {
        
        static var hostName: String {
            return "api.github.com"
        }

        static var baseURI: String {
            return "https://" + hostName
        }
    }
    
    static var mainViewController: UIViewController? {
        return UIApplication
            .shared
            .connectedScenes
            .flatMap { ($0 as? UIWindowScene)?.windows ?? [] }
            .first { $0.isKeyWindow }?.rootViewController
    }
    
    static var topMostController: UIViewController? {
        var topController: UIViewController? = mainViewController
        while topController?.presentedViewController != nil {
            topController = topController?.presentedViewController
        }
        return topController
    }
}
