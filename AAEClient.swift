import Foundation
import Alamofire

class AAEClient {
    struct Setting {
        let baseURL: String
        var accessToken: String?
        let manager: Session
        init(baseURL: String, manager: Session) {
            self.baseURL = baseURL
            self.manager = manager
        }
    }
    private(set) var setting: Setting
    var accessToken: String? {
        get {
            return setting.accessToken
        }
        set {
            setting.accessToken = newValue
        }
    }
    
    
    var refetchAuthCode: ((@escaping (String) -> Void) -> Void)?
    var serverMaintenanceOccured: (() -> Void)?
    var endOfService: (() -> Void)?
    // MARK: - Init

    init(setting: Setting) {
        self.setting = setting
    }
    convenience init(baseURL: String, timeout: TimeInterval = 10, maximumConnections: Int? = nil) {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = timeout
        if let maximumConnections = maximumConnections {
            configuration.httpMaximumConnectionsPerHost = maximumConnections
        }
        let manager = Session(configuration: configuration)
        let setting = Setting(baseURL: baseURL, manager: manager)
        self.init(setting: setting)
    }
    
    // MARK: - Public
    func fetchUserList(_ parameters: AAEUserListParameters, completion: @escaping (AAEResult<[AAEUser], AAEError>) -> Void) {
        AAEEndpoint.FetchUserList.request(self, method: .get, parameters: parameters, completion: completion)
    }
    
    func fetchUserDetail(_ login: String, completion: @escaping (AAEResult<AAEUserDetail, AAEError>) -> Void) {
        AAEEndpoint.FetchUserDetail(login: login).request(self, method: .get, parameters: nil, completion: completion)
    }
}
